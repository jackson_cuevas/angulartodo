import { AngularFireAuth } from '@angular/fire/auth';
import { Component, NgZone ,OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  user: User;
  constructor(private afAuth: AngularFireAuth,
    private router: Router,
    private ngZone: NgZone) { }

  ngOnInit() {
    this.afAuth.auth.onAuthStateChanged(user => {
      if (user) {
        this.user = user;
      } else {
        this.user = null;
        this.ngZone.run(() => {
          this.router.navigate(['']);
        })
      }
    });
  }
}
