import { AngularFireAuth } from '@angular/fire/auth';
import { TodoViewModel } from './../models/todo-view-model';
import { Todo } from './../models/todo';
import { DocumentReference } from '@angular/fire/firestore';
import { TodoService } from './../services/todo.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { User } from 'firebase';

@Component({
  selector: 'app-todo-form',
  templateUrl: './todo-form.component.html',
  styleUrls: ['./todo-form.component.css']
})
export class TodoFormComponent implements OnInit {
  todoForm: FormGroup;
  createMode: boolean = true;
  todo: TodoViewModel;
  user: User;

  constructor(private formBuilder: FormBuilder,
    public activeModal: NgbActiveModal,
    private todoService: TodoService,
    private afAuth: AngularFireAuth) { }

  ngOnInit() {
    this.todoForm = this.formBuilder.group({
      title: ['', Validators.required],
      description: ['', Validators.required],
      done: false
    });

    if (!this.createMode) { this.loadTodo(this.todo); }

    this.afAuth.user.subscribe(user => {
      if (user) {
        this.user = user;
      }
    })
  }

  loadTodo(todo) {
    this.todoForm.patchValue(todo);
  }

  saveTodo() {
    //Validar el formulario

    if (this.todoForm.invalid) {
      return;
    }

    //Enviar la informacion hacia Firebase
    if(this.createMode) {
    let todo: Todo = this.todoForm.value;
    todo.lastModifiedDate = new Date();
    todo.createdDate = new Date();
    todo.userId = this.user.uid;
    this.todoService.saveTodo(todo)
      .then(response => this.handleSuccessfulSaveTodo(response, todo))
      .catch(err => console.error(err));
    }
  }

  handleSuccessfulSaveTodo(response: DocumentReference, todo: Todo) {
    //Enviar la informacion al todo-list
    this.activeModal.dismiss({ todo: todo, id: response.id, createMode: true });
  }

  handleSuccessfulEditTodo(todo: TodoViewModel) {
    this.activeModal.dismiss({ todo: todo, id: todo.id, createMode: false });
  }

}
