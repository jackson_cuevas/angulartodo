import { AngularFireAuth } from '@angular/fire/auth';
import { User } from 'firebase';
import { Component, Input} from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent{
  @Input()
  user: User;
  constructor(private afAuth: AngularFireAuth) { }

  logout() {
    this.afAuth.auth.signOut();
  }

}
